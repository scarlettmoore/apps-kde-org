# SPDX-FileCopyrightText: 2017 Harald Sitter <sitter@kde.org>
# SPDX-FileCopyrightText: 2022 Phu Hung Nguyen <phu.nguyen@kdemail.net>
# SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL

from __future__ import annotations

import configparser
import os
import sys

from enum import Enum, EnumMeta

from .xdg_unit import XDGUnit


class IconTheme(XDGUnit):
    subdir = 'icons'
    _icon_dirs: list[str] = []

    class DirectoryTypeMeta(EnumMeta):
        def __contains__(cls, item):
            try:
                cls(item)
            except ValueError:
                return False
            else:
                return True

    class DirectoryType(str, Enum, metaclass=DirectoryTypeMeta):
        FIXED = 'Fixed'
        SCALABLE = 'Scalable'
        THRESHOLD = 'Threshold'

    class Directory:
        def __init__(self, path, cfg):
            self.path = path
            self.size = int(cfg['Size'])
            self.scale = int(cfg.get('Scale', 1))
            self.context = cfg['Context']
            self.type: str = cfg.get('Type', IconTheme.DirectoryType.THRESHOLD)
            self.max_size = int(cfg.get('MaxSize', self.size))
            self.min_size = int(cfg.get('MinSize', self.size))
            self.threshold = cfg.get('Threshold', 2)

        def __str__(self):
            return self.path

        def matches_size(self, icon_size: int, icon_scale: int):
            assert self.type in IconTheme.DirectoryType, f'unknown type {self.type}'
            if icon_scale != self.scale:
                return False

            if self.type == IconTheme.DirectoryType.FIXED:
                return self.size == icon_size
            elif self.type == IconTheme.DirectoryType.SCALABLE:
                return self.min_size <= icon_size <= self.max_size
            else:  # 'Threshold'
                return self.size - self.threshold <= icon_size <= self.size + self.threshold

        def size_distance(self, icon_size: int, icon_scale: int):
            assert self.type in IconTheme.DirectoryType, f'unknown type {self.type}'

            scaled_icon_size = icon_size * icon_scale
            scaled_subdir_size = self.size * self.scale
            scaled_subdir_min = self.min_size * self.scale
            scaled_subdir_max = self.max_size * self.scale

            if self.type == IconTheme.DirectoryType.FIXED:
                return abs(scaled_subdir_size - scaled_icon_size)
            elif self.type == IconTheme.DirectoryType.SCALABLE:
                return scaled_subdir_min - scaled_icon_size if scaled_icon_size < scaled_subdir_min \
                    else scaled_icon_size - scaled_subdir_max if scaled_icon_size > scaled_subdir_max \
                    else 0
            else:  # 'Threshold'
                lower_threshold = (self.size - self.threshold) * self.scale
                upper_threshold = (self.size + self.threshold) * self.scale
                return scaled_subdir_min - scaled_icon_size if scaled_icon_size < lower_threshold \
                    else scaled_icon_size - scaled_subdir_max if scaled_icon_size > upper_threshold \
                    else 0

    @classmethod
    def icon_dirs(cls):
        if not cls._icon_dirs:
            cls._icon_dirs = [os.path.join(os.path.expanduser('~'), '.icons')] + \
                             cls.xdg_data_dirs() + \
                             ['/usr/share/pixmaps']
        return cls._icon_dirs

    def __init__(self, name, extra_data_dirs=None):
        if extra_data_dirs is None:
            extra_data_dirs = []

        self.name = name
        data_dirs = extra_data_dirs + self.icon_dirs()
        # These are the toplevel theme dir locations /usr/share/icons/foo etc.
        self.dirs = [os.path.join(path, name) for path in data_dirs]
        # The dirs which were defined as extra on top of the defaults
        # This is a subset of dirs!
        self.extra_data_dirs = extra_data_dirs

        # Config object
        self.config = self._load_config()
        if self.config is None:
            raise ValueError(f'Could not resolve theme {name} {repr(self)}')
        theme_config = self.config['Icon Theme']
        self.parents = []
        if 'Inherits' in theme_config:
            self.parents = [IconTheme(x, extra_data_dirs) for x in theme_config['Inherits'].split(',')]
        subdirs = theme_config['Directories']
        if 'ScaledDirectories' in theme_config:
            subdirs += ',' + theme_config['ScaledDirectories']
        self.subdirs = list(dict.fromkeys([IconTheme.Directory(d, self.config[d]) for d in subdirs.split(',') if d]))

    def is_valid(self):
        return any(os.path.exists(d) and os.path.exists(os.path.join(d, 'index.theme')) for d in self.dirs)

    def _load_config(self):
        directory = next((x for x in self.dirs if os.path.exists(x) and
                          os.path.exists(os.path.join(x, 'index.theme'))), '')
        if not directory:
            return None

        cp = configparser.RawConfigParser()
        # By default, keys are lowercased. This makes it keep the case
        cp.optionxform = str
        cp.read(os.path.join(directory, 'index.theme'))
        return cp


class IconLoader:
    EXTENSIONS = ['svg']

    def __init__(self, icon: str, size: int, theme: IconTheme, scale=1):
        self.icon = icon
        self.size = size
        self.theme = theme
        self.scale = scale

    def hicolor_theme(self):
        return IconTheme('hicolor', self.theme.extra_data_dirs)

    def find_icon(self):
        file_path = self.find_icon_in_theme(self.theme)
        if file_path:
            return file_path
        file_path = self.find_icon_in_theme(self.hicolor_theme())
        if file_path:
            return file_path
        return self.lookup_fallback_icon()

    def find_icon_in_theme(self, theme: IconTheme) -> str:
        file_path = self.lookup_icon(theme)
        if file_path:
            return file_path
        for parent in theme.parents:
            file_path = self.find_icon_in_theme(parent)
            if file_path:
                return file_path
        return ''

    def lookup_icon(self, theme: IconTheme) -> str:
        for d in theme.dirs:
            for subdir in theme.subdirs:
                if subdir.matches_size(self.size, self.scale):
                    for ext in IconLoader.EXTENSIONS:
                        file_path = f'{d}/{subdir}/{self.icon}.{ext}'
                        if os.path.exists(file_path):
                            return file_path

        closest_file = ''
        minimal_size = sys.maxsize
        for d in theme.dirs:
            for subdir in theme.subdirs:
                for ext in IconLoader.EXTENSIONS:
                    file_path = f'{d}/{subdir}/{self.icon}.{ext}'
                    if os.path.exists(file_path):
                        dist = subdir.size_distance(self.size, self.scale)
                        if dist < minimal_size:
                            closest_file = file_path
                            minimal_size = dist
        return closest_file

    def lookup_fallback_icon(self) -> str:
        for d in self.theme.dirs:
            for ext in IconLoader.EXTENSIONS:
                file_path = f'{d}/{self.icon}.{ext}'
                if os.path.exists(file_path):
                    return file_path
        return ''


class Icon:
    @classmethod
    def find_path(cls, icon: str, size: int, theme: IconTheme, scale=1):
        return IconLoader(icon, size, theme, scale).find_icon()
