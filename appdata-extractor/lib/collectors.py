# SPDX-FileCopyrightText: 2017-2018 Harald Sitter <sitter@kde.org>
# SPDX-FileCopyrightText: 2022 Phu Hung Nguyen <phu.nguyen@kdemail.net>
# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

import glob
import json
import logging
import os.path

from hugoi18n.generation import convert_lang_code

from . import category
from .appdata import AppData
from .icon_fetcher import IconFetcher
from .kde_project import Project
from .unmaintained import Unmaintained
from .xdg.desktop import ApplicationDesktopLoader, Desktop
from .xdg.icon import IconTheme


class CollectorException(Exception):
    pass


class AppstreamCollector:
    APPLICATION_TYPES = ['desktop', 'desktop-application', 'console-application',
                         'web-application', 'addon']

    @staticmethod
    def _lang_code_func(lang_code):
        return 'en' if lang_code == 'C' else convert_lang_code(lang_code)

    def __init__(self, directory, path, project: Project, theme: IconTheme):
        self.directory = directory
        self.path = path
        self.project = project
        self.appdata = AppData(self.path).read_serialize(self._lang_code_func)
        # Sanitize for our purposes (can be either foo.desktop or foo, we always use the latter internally).
        # The value in the appdata must stay as it is or appstream:// urls do not work!
        self.appid = self.appdata['ID'].replace('.desktop', '')
        # WARNING do not mutate ID in appdata!
        # libappstream is too daft to handle foo.desktop and foo the same, so for external purposes we need
        # to keep the input ID. This is particularly important as to not break expectations when handling
        # our data blobs e.g. to build appstream:// URIs in the frontend code. Instead, we'll pack our
        # mutated id into a special key we can later use to also uniquely identify things in the frontend.
        # Use the name 'D_APP_ID' for the real app ID
        # Our ID (without '.desktop') takes 'ID'
        self.appdata['D_APP_ID'] = self.appdata['ID']
        self.appdata['ID'] = self.appid

        artifact_dir = [f'{directory}/share/icons'] if os.path.isdir(f'{directory}/share/icons') else []
        self.icon_theme = IconTheme(theme.name, theme.extra_data_dirs + artifact_dir)
        self.desktop_file = None

    def xdg_data_dir(self):
        return f'{self.directory}/share'

    def get_desktop_file(self):
        if not self.desktop_file:
            # Special case: kdeconnect
            appid = self.appid + '.app' if self.appid == 'org.kde.kdeconnect' else self.appid
            search_dirs = [f'{self.xdg_data_dir()}/applications']
            # Special case: konqueror
            if self.appid == 'org.kde.konqueror':
                search_dirs.append(f'{self.xdg_data_dir()}/kservices5')
            local_id = Unmaintained.with_local_id(appid.split('.', 2)[2])
            # use 'deskto' extension to prevent scripty from processing our local desktop files
            self.desktop_file = (ApplicationDesktopLoader(appid, search_dirs).find()
                                 if not local_id else Desktop(f'{Unmaintained.local_storage}/{local_id}.deskto'))
        return self.desktop_file

    def is_desktop_app(self):
        # is type desktop or desktop-application
        return self.appdata['Type'].startswith('desktop')

    def _get_icon_name(self):
        if not self.is_desktop_app():
            if 'Icon' in self.appdata and 'stock' in self.appdata['Icon']:
                icon_name = self.appdata['Icon']['stock']
            else:  # addon for now
                icon_name = self.appdata.get('Icon', 'folder-build')
        # special cases: kookbook, ksame
        elif self.appid == 'org.kde.kookbook':
            icon_name = 'sc-apps-kookbook'
        elif self.appid == 'org.kde.ksame':
            icon_name = 'klickety'
        else:
            icon_name = self.get_desktop_file().icon
        return icon_name

    def grab_icon(self):
        icon_name = self._get_icon_name()
        IconFetcher(icon_name, self.icon_theme).extend_appdata(self.appdata, self.appid)

    # qrca: desktop file sets the category to 'Multimedia', we replace that by 'AudioVideo'
    # gotten from edu.k.o: blinken, gcompris, kgeography: misc; step: science
    categories_special_cases = {'blinken': ['X-KDE-Edu-Misc'],
                                'gcompris': ['X-KDE-Edu-Misc'],
                                'kairo': ['Utility'],
                                'kgeography': ['X-KDE-Edu-Misc'],
                                'khelpcenter': ['System'],
                                'kitinerary-extractor': ['Office', 'Utility'],
                                'liquidshell': ['System'],
                                'pikasso': ['Graphics'],
                                'qrca': ['AudioVideo'],
                                'rkward': ['Education'],
                                'rust_qt_binding_generator': ['Development'],
                                'step': ['Science']}

    def grab_categories(self):
        app_short_id = self.appid.split('.', 2)[2]
        # Special cases: those in the list above
        #   and addons 'okular-'
        if app_short_id in self.categories_special_cases:
            categories = self.categories_special_cases[app_short_id]
        elif app_short_id.startswith('okular-'):
            categories = ['Office']
        else:
            categories = [] if 'Categories' not in self.appdata else self.appdata['Categories']
        # If the categories are defined in both desktop and appdata files, combine them
        if self.is_desktop_app():
            categories += self.get_desktop_file().categories
        # Make sure to filter out all !main categories.
        subcategories = list(set(categories) & set(category.MAIN_SUBCATEGORIES))
        self.appdata['subcategories'] = sorted([category.to_code(x) for x in subcategories])
        categories = list(set(categories) & set(category.MAIN_CATEGORIES))
        self.appdata['Categories'] = sorted([category.to_code(x) for x in categories])

    def grab_generic_name(self):
        generic_name = {}
        if self.is_desktop_app():
            generic_name = self.get_desktop_file().localized('GenericName', self._lang_code_func)
        if not generic_name:
            generic_name = self.appdata['Summary']
        self.appdata['X-KDE-GenericName'] = generic_name

    def grab_others(self):
        self.appdata['bug_list'] = self.project.bug_list
        self.appdata['bug_report'] = self.project.bug_report
        self.appdata['project_identifier'] = self.project.identifier
        self.appdata['X-KDE-Repository'] = self.project.repo

    def grab_app_data(self, non_apps=None):
        if non_apps is None:
            non_apps = set()

        app_type = self.appdata.get('Type')
        if app_type not in self.APPLICATION_TYPES:
            logging.warning(f'{self.appid} is not an application')
            return False
        # skip plasma addons and stuff we don't want to display
        if (app_type == 'addon' and self.appid.startswith('org.kde.plasma')) or self.appid in non_apps:
            return False
        # addons have no desktop files
        if self.is_desktop_app():
            if not self.get_desktop_file():
                raise CollectorException(f'No desktop file for {self.appid}')
            if not self.get_desktop_file().show_in('KDE'):
                raise CollectorException(f'Desktop file for {self.appid} is not meant for display')

        logging.info(f'Processing app {self.appid}')
        # thumbnails are not used anymore
        self.grab_icon()
        self.grab_categories()
        self.grab_generic_name()
        self.grab_others()

        with open(f"../static/appdata/{self.appid}.json", 'w') as f_json:
            json.dump(self.appdata, f_json, ensure_ascii=False, indent=2)

        # FIXME: we should put EVERYTHING into a well defined tree in a tmpdir,
        #   then move it into place in ONE place. so we can easily change where
        #   stuff ends up in the end and know where it is while we are working on
        #   the data
        return True

    @classmethod
    def grab_project_data(cls, directory, project: Project, theme=None, local_id='', non_apps=None):
        """
        Collect data of apps in the project
        :param directory: containing the project
        :param project: the Project object
        :param theme: the icon theme to use for icon searching
        :param local_id: id of the matching local unmaintained project, empty if not existing
        :param non_apps: ids of apps to be considered as non apps
        :return: a bool indicating whether this project is considered to be processed or not
            - False: when no appdata is found or exceptions are raised for all found ones
            - True: when at least one grab_app_data returns a value
        """
        if local_id:
            # use '.appdata' instead of '.appdata.xml' to prevent scripty from processing our local appdata files
            paths = glob.glob(f'{Unmaintained.local_storage}/{local_id}.appdata')
        else:
            # metainfo.xml is the right extension to use unless you're an
            # app and don't want to use that one in which case use appdata.xml
            paths = glob.glob(f'{directory}/**/**.appdata.xml', recursive=True)
            paths += glob.glob(f'{directory}/**/**.metainfo.xml', recursive=True)
        paths = filter(lambda p: 'org.example' not in p, paths)
        results = []
        for path in paths:
            logging.info(f'Grabbing app data at {path}')
            try:
                result = cls(directory, path, project, theme).grab_app_data(non_apps)
                results.append(result)
            except CollectorException as e:
                logging.warning(e)
        return len(results) > 0


class GitAppstreamCollector(AppstreamCollector):
    def get_desktop_file(self):
        if self.desktop_file:
            return self.desktop_file
        # special cases
        if self.project.identifier == 'kexi':
            appid = 'org.kde.kexi'
        else:
            appid = self.appid
        files = glob.glob(f'{self.directory}/**/{appid}.desktop', recursive=True)
        files += glob.glob(f'{self.directory}/**/{appid}.desktop.in', recursive=True)
        substrings_to_exclude = ['snap/setup', 'snap/gui', 'APPNAMELC', 'org.example']
        files = list(filter(lambda f: all(map(lambda s: s not in f, substrings_to_exclude)), files))
        if len(files) != 1:
            raise Exception(f'{appid}: {repr(files)}')
        logging.info(f'Found desktop at {files[0]}')
        self.desktop_file = Desktop(files[0])
        return self.desktop_file

    def grab_icon(self):
        # try to find svg icon in theme first, then in tree
        icon_name = self._get_icon_name()
        if not IconFetcher(icon_name, self.icon_theme).extend_appdata(self.appdata, self.appid):
            icon_paths = glob.glob(f'{self.directory}/**/{icon_name}.svg', recursive=True)
            if len(icon_paths) >= 1:
                file_name = self.appid + '.svg'
                IconFetcher.copy_file(icon_paths[0], file_name)
                IconFetcher.write_data(self.appdata, file_name)
