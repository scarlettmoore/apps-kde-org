# SPDX-FileCopyrightText: 2017 Harald Sitter <sitter@kde.org>
# SPDX-FileCopyrightText: 2022 Phu Hung Nguyen <phu.nguyen@kdemail.net>
# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

import logging
import shutil

from .xdg.icon import Icon, IconTheme


class IconFetcher:
    def __init__(self, icon_name, theme: IconTheme):
        self.icon_name = icon_name
        self.theme = theme

    @classmethod
    def copy_file(cls, src: str, dst_name: str, dst_subdir=''):
        dst = f'../static/app-icons/{dst_subdir}{dst_name}'
        logging.info(f'Copying {src} to {dst}')
        shutil.copy2(src, dst)

    @classmethod
    def write_data(cls, data: dict, file_name: str):
        data['Icon'] = {'local': [{'name': file_name}]}

    def extend_appdata(self, data: dict, main_name: str, subdir=''):
        if not main_name:
            logging.info('An empty icon name! Use planetkde.org')
            file_name = 'planetkde.svg'
        else:
            logging.info(f'Looking for {self.icon_name} icon...')
            icon_path = Icon.find_path(self.icon_name, 48, self.theme)
            if icon_path:
                file_name = main_name + '.svg'
                self.copy_file(icon_path, file_name, subdir)
            else:  # use this for all desktop apps without an icon
                file_name = 'planetkde.svg'

        self.write_data(data, file_name)
        return file_name != 'planetkde.svg'
