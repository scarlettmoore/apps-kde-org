# SPDX-FileCopyrightText: 2017-2020 Harald Sitter <sitter@kde.org>
# SPDX-FileCopyrightText: 2022 Phu Hung Nguyen <phu.nguyen@kdemail.net>
# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

import asyncio
import concurrent.futures
import logging
import os
import shutil
import subprocess
import tempfile

from lib import category
from lib.collectors import AppstreamCollector, GitAppstreamCollector
from lib.icon_fetcher import IconFetcher
from lib.ci_utilities import Registry
from lib.kde_project import Project
from lib.unmaintained import Unmaintained
from lib.xdg.desktop import DesktopDirectoryLoader
from lib.xdg.icon import IconTheme
from nio import AsyncClient

if __name__ == "__main__":
    level = logging.INFO
    logging.basicConfig(format='%(levelname)s: %(message)s', level=level)

    repos = Project.list_repos()
    filtered = []

    # repos starting with one of these are excluded
    starting_strings_non_apps = ['documentation/', 'frameworks/', 'historical/', 'libraries/', 'neon/',
                                 'network/kaccounts-', 'network/ktp-', 'packaging/', 'plasma-bigscreen/', 'sysadmin/',
                                 'webapps/', 'websites/', 'wikitolearn/']

    # specific repos of apps in each group
    # many Maui apps are not yet ready
    maui_apps = {'index-fm', 'nota', 'pix', 'vvave'}
    pim_apps = {'akregator', 'itinerary', 'kaddressbook', 'kalarm', 'kalendar', 'kitinerary', 'kleopatra', 'kmail',
                'knotes', 'kontact', 'korganizer', 'ktimetracker', 'pim-data-exporter',
                'trojita', 'vakzination', 'zanshin'}
    # Plasma has its own web page, include only some projects
    plasma_apps = {'discover', 'plasma-systemmonitor', 'ksysguard'}
    plasma_mobile_apps = {'calindori',
                          'plasma-camera', 'plasma-dialer', 'plasma-phonebook', 'qmlkonsole',
                          'spacebar'}
    # only rolisteam-packaging contains an app
    rolisteam_apps = {'rolisteam-packaging'}
    system_apps = {'apper', 'dolphin', 'kcron', 'khelpcenter', 'ksystemlog', 'kup', 'liquidshell', 'muon',
                   'partitionmanager', 'wacomtablet', 'kjournald'}
    opted_in_app_groups = {'maui/': maui_apps, 'pim/': pim_apps,
                           'plasma/': plasma_apps, 'plasma-mobile/': plasma_mobile_apps,
                           'rolisteam/': rolisteam_apps, 'system/': system_apps}

    # individual repos of non-apps
    plasmoids = {'alkimia'}
    # libraries: analitza,
    # actual non-apps: atelier, kdialog, print-manager,
    # archived repos: totalreqall,
    # WIP: arkade,
    repos_non_apps = {'analitza', 'arkade', 'atelier', 'kdialog',
                      'print-manager',
                      'totalreqall'} | plasmoids
    # individual ids of non-apps: amarok, keurocalc, marble
    non_apps = {'org.kde.amarok.albums', 'org.kde.amarok.photos', 'org.kde.curconvd',
                'org.kde.marble.behaim', 'org.kde.marble.maps'}
    # for testing
    testing_apps = {'ghostwriter'}

    # filter repos
    for repo in repos:
        # some groups are excluded altogether
        if any(map(lambda x: repo.startswith(x), starting_strings_non_apps)):
            continue
        repo_basename = os.path.basename(repo)

        should_skip = False
        for repo_start, included_basenames in opted_in_app_groups.items():
            if repo.startswith(repo_start) and repo_basename not in included_basenames:
                should_skip = True
                break
        if should_skip:
            continue

        # We do want some unmaintained bits for backwards compatibility
        #   hidden away at https://apps.kde.org/unmaintained/
        if repo_basename in Unmaintained.projects_w_repo:
            filtered.append(repo)
            continue
        elif repo.startswith('unmaintained/'):
            continue
        # individual non-apps, data repos and lib repos
        if repo_basename in repos_non_apps or repo_basename.endswith('-data') or repo_basename.startswith('lib'):
            continue

        # if repo_basename not in testing_apps:
        #     continue
        # other than those, include all
        filtered.append(repo)
    repos = filtered + Unmaintained.projects_wo_repo
    Project.gen_cache(repos)

    GITLAB_INSTANCE = 'https://invent.kde.org'
    SUSE_PROJECT = 'teams/ci-artifacts/suse-qt5.15'
    SUSE_CACHE_PATH = '/mnt/artifacts/suse-qt5.15/' if os.environ.get('CI_COMMIT_REF_NAME') else '/srv/archives/'
    # SUSE_CACHE_PATH = 'archives'

    suse_registry = Registry(SUSE_CACHE_PATH, GITLAB_INSTANCE, SUSE_PROJECT)

    # Download breeze-icons for getting icons and plasma-workspace for getting app menu categories
    SUPPORTS = [{'basename': 'breeze-icons', 'branch': 'kf5'}, {'basename': 'plasma-workspace', 'branch': 'master'}]

    for project in SUPPORTS:
        if os.path.isdir(project['basename']):
            continue
        suse_registry.retrieve_install(project['basename'], project['basename'], project['branch'])

    for project in SUPPORTS:
        if not os.path.isdir(project['basename']):
            basename = project['basename']
            raise Exception(f'Failed to get {basename}! Resolution of CI artifacts failed!')

    local_breeze = IconTheme('breeze', ['extra-icons', 'breeze-icons/share/icons'])

    for cat, desktop_id in {**category.CATEGORY_DESKTOPS_MAP, **category.SUBCATEGORY_DESKTOPS_MAP}.items():
        loader = DesktopDirectoryLoader(desktop_id, ['plasma-workspace/share/desktop-directories'])
        desktop = loader.find()
        if not desktop:
            raise Exception(f'No desktop file for {desktop_id}')
        name = category.to_code(cat)
        icon_name = desktop.desktop_config['Icon']
        IconFetcher(icon_name, local_breeze).extend_appdata({}, name, 'categories/')

    # process non-local projects, put the rest into non_local
    non_local = []
    for repo in repos:
        if local_id := Unmaintained.with_local_id(os.path.basename(repo)):
            AppstreamCollector.grab_project_data('.', Project.get(repo), theme=local_breeze, local_id=local_id)
        else:
            non_local.append(repo)
    repos = non_local
    # now repos contains only non-local stuff, we try to get them from CI or Git

    problematic_repos = []
    with tempfile.TemporaryDirectory() as tmp_dir:
        def process_project(p_repo: str):
            p = Project.get(p_repo)
            p_tmp_dir = os.path.join(tmp_dir, p.identifier)
            os.makedirs(p_tmp_dir, exist_ok=True)
            try:
                suse_registry.retrieve_install(p.identifier, p_tmp_dir, 'master')
                logging.info(f'Processing project {p_tmp_dir}')
                ci_result = AppstreamCollector.grab_project_data(p_tmp_dir, p, theme=local_breeze, non_apps=non_apps)
            except Exception as p_e:
                logging.warning('Project %r CI processing generated an exception (%s): %s' % (p_repo, type(p_e), p_e))
                if f'{p_e}' != 'nothing to open':
                    problematic_repos.append(p_repo)
                ci_result = False
            shutil.rmtree(p_tmp_dir)
            # after CI processing, skip Git processing with leftover repos in kdevelop group
            if ci_result or p_repo.startswith('kdevelop/'):
                repos.remove(p_repo)
            else:
                # FIXME: cache and git pull?
                os.makedirs(p_tmp_dir, exist_ok=True)
                command_parts = ['git', 'clone', '--depth', '1',
                                 f'https://invent.kde.org/{p_repo}.git', p_tmp_dir]
                logging.info(f'{command_parts}')
                subprocess.run(command_parts)
                git_result = GitAppstreamCollector.grab_project_data(p_tmp_dir, p,
                                                                     theme=local_breeze, non_apps=non_apps)
                shutil.rmtree(p_tmp_dir)
                if git_result:
                    repos.remove(p_repo)

        with concurrent.futures.ThreadPoolExecutor(max_workers=8) as executor:
            # Mark each future with its project
            future_to_project = {executor.submit(process_project, repo): repo for repo in repos}
            for future in concurrent.futures.as_completed(future_to_project):
                repo = future_to_project[future]
                try:
                    future.result()
                except Exception as e:
                    logging.warning('Project %r generated an exception (%s): %s' % (repo, type(e), e))
                else:
                    logging.info(f'Processed project {repo}')

    logging.info(f'Not processed: {repos}')

    # send a message to notify about problematic projects when all are true:
    # - access token is available (i.e. on master only)
    # - there are some problematic repos
    # - we are on GitLab CI (i.e. do not send from Jenkins)
    if 'MATRIX_ACCESS_TOKEN' in os.environ and problematic_repos and 'CI_COMMIT_REF_NAME' in os.environ:
        credentials = {
            'homeserver': 'https://kde.modular.im',
            'access_token': os.environ['MATRIX_ACCESS_TOKEN'],
            'user_id': '@build-notifier:kde.org',
            'device_id': 'NGQQRFKCBO'
        }
        # kde-www room
        room_id = '!KWPxbUviPXLYlJAlCm:matrix.org'
        problematic_repos.sort()
        problematic_repos = '\n'.join(problematic_repos)
        body_start = '[apps.kde.org] the last build had problems with following projects:\n'
        formatted_body = f'{body_start}<pre><code>{problematic_repos}</code></pre>'
        body = f'{body_start}```\n{problematic_repos}\n```'

        async def send_message() -> None:
            client = AsyncClient(credentials['homeserver'])
            client.access_token = credentials['access_token']
            client.user_id = credentials['user_id']
            client.device_id = credentials['device_id']
            await client.room_send(room_id,
                                   message_type='m.room.message',
                                   content={'msgtype': 'm.text',
                                            'format': 'org.matrix.custom.html',
                                            'body': body,
                                            'formatted_body': formatted_body})
            await client.close()
        asyncio.get_event_loop().run_until_complete(send_message())
