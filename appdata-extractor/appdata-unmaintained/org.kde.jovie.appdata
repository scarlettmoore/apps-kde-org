<?xml version="1.0" encoding="utf-8"?>
<component type="desktop">
  <id>org.kde.jovie</id>
  <metadata_license>CC0-1.0</metadata_license>
  <project_license>GPL-2.0+</project_license>
  <name>Jovie</name>
  <name xml:lang="nl">Jovie</name>
  <name xml:lang="uk">Jovie</name>
  <summary>Jovie KDE Text-to-Speech is a subsystem within the KDE desktop for conversion of text to audible speech. Jovie is currently under development and aims to become the standard subsystem for all KDE applications to provide speech output.</summary>
  <summary xml:lang="uk">Система синтезу мовлення KDE Jovie є підсистемою у стільниці KDE, яку призначено для озвучення тексту. Jovie перебував у стані розробки. Метою є створення стандартної підсистеми для усіх програм KDE і забезпечення озвучення у всіх цих програмах.</summary>
  <description>
    <p>User Features:</p>
    <p xml:lang="uk">Можливості для користувача:</p>
    <ul>
      <li>Speak any text from the KDE clipboard</li>
      <li xml:lang="uk">Озвучення будь-якого тексту з буфера обміну даними KDE</li>
      <li>Speak any plain text file</li>
      <li xml:lang="uk">Озвучення будь-якого звичайного текстового файла</li>
      <li>Speak all or any portion of a text file from Kate, including instances where Kate is embedded in another KDE application</li>
      <li xml:lang="uk">Озвучення усього або частини текстового файла у Kate, включно з екземплярами, де Kate є вбудованою до інших програм KDE</li>
      <li>Speak all or any portion of an HTML page from Konqueror</li>
      <li xml:lang="uk">Озвучення будь-якої частини сторінки HTML у Konqueror</li>
      <li>Use as the speech backend for KMouth and KSayIt</li>
      <li xml:lang="uk">Можливість використання як модуля озвучення у KMouth і KSayIt</li>
      <li>Speak KDE notifications (KNotify)</li>
      <li xml:lang="uk">Озвучення сповіщень KDE (KNotify)</li>
      <li>User-configurable filters for substituting misspoken words, choosing speech synthesizers, and transforming XHMTL/XML documents</li>
      <li xml:lang="uk">Придатні до налаштовування користувачем фільтри для заміни слів із помилками, вибору синтезаторів мовлення та перетворення документів XHMTL/XML</li>
    </ul>
    <p>Programmer Features:</p>
    <p xml:lang="uk">Можливості для програмістів:</p>
    <ul>
      <li>Priority system for screen reader outputs, warnings and messages, while still playing regular texts</li>
      <li xml:lang="uk">Система пріоритетів для виведення засобів читання з екрана, попереджень і повідомлень під час відтворення звичайних текстів</li>
      <li>Permit generation of speech from the command line (or via shell scripts) using the qdbus utility</li>
      <li xml:lang="uk">Можливість синтезу мовлення з командного рядка (або зі скриптів оболонки) з використанням допоміжного засобу qdbus</li>
      <li>Provide a lightweight and easily usable interface for applications to generate speech output</li>
      <li xml:lang="uk">Надання спрощеного і простого у користуванні інтерфейсу для програм для синтезу звукових даних з тексту</li>
      <li>Applications need not be concerned about contention over the speech device</li>
      <li xml:lang="uk">Сповіщення програм щодо конфлікту за пристрій для озвучення</li>
      <li>FUTURE: Provide support for speech markup languages, such as VoiceXML, Sable, Java Speech Markup Language (JSML), and Speech Markup Meta-language (SMML)</li>
      <li xml:lang="uk">У майбутньому: забезпечення підтримки озвучення коду мов розмітки, зокрема VoiceXML, Sable, Java Speech Markup Language (JSML) та Speech Markup Meta-language (SMML)</li>
      <li>FUTURE: Provide limited support for embedded speech markers</li>
      <li xml:lang="uk">У майбутньому: обмежена підтримка для вбудованих позначок озвучення</li>
      <li>Asynchronous to prevent system blocking</li>
      <li xml:lang="uk">Асинхронна робота для запобігання блокуванню системи</li>
    </ul>
  </description>
  <url type="homepage">http://www.kde.org</url>
  <url type="donation">https://www.kde.org/community/donations/?app=jovie&amp;source=appdata</url>
  <screenshots>
    <screenshot type="default">
      <image>https://kde.org/images/screenshots/jovie.png</image>
    </screenshot>
  </screenshots>
  <project_group>KDE</project_group>
  <provides>
    <binary>jovie</binary>
  </provides>
  <launchable type="desktop-id">org.kde.jovie</launchable>
</component>
