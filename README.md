# KDE Apps website
[![Build Status](https://binary-factory.kde.org/buildStatus/icon?job=Website_apps-kde-org)](https://binary-factory.kde.org/job/Website_apps-kde-org/)

This is [apps.kde.org](https://apps.kde.org), a Hugo-based website displaying all the applications made by the KDE community.

## How it works
An extractor extracts all KDE projects' AppStream metadata to JSON files which are then converted by a converter to Markdown files.
Hugo uses these Markdown files to generate webpages.

### Extractor
The extractor resides in `appdata-extractor` folder.
- It filters projects from https://projects.kde.org/api/ (which is made from [repo-metadata](https://invent.kde.org/sysadmin/repo-metadata/));
- It gets artifacts of the filtered projects from the CI system and extracts data from AppStream files (org.kde.foo.appstream.xml)
  and Desktop files (org.kde.foo.desktop) in those artifacts into JSON files;
- If no data is found, it downloads projects' Git repos and extracts those from there;
- JSON files are stored in `static/appdata` folder. They are hosted at https://apps.kde.org/appdata;
- It also looks for category and app icons, then copies them to `static/app-icons` folder.
  These icons similarly can be found at https://apps.kde.org/app-icons.

### Converter
The `main.py` module acts as the converter. It converts JSON to Markdown files.
- The app list on the home page by default is sorted by apps' popularity.
  The converter calculates popularity points based on data of the previous month from [Flathub](https://flathub.org/stats/) and [Arch Linux](https://pkgstats.archlinux.de/);
- A language is included in the website when there are at least 50 apps with the AppStream `summary` field translated into the language;
- The converter also generates files to make up the structure of the Hugo site.

### Hugo
- `category` and `platform` are used as taxonomies.
  There are 9 `category` terms corresponding to 9 app categories in KDE, and currently 4 `platform` terms: `android`, `ios`, `macos`, and `windows`.
  `unmaintained` is technically a `category` too, but it is hidden.
- Additional descriptions for apps in the `games` category and additional contact information for all apps are stored as Hugo data.

### Unmaintained apps
Some apps were on this site before, then became unmaintained. Their links should not disappear, but they should not appear as active apps either.
So we have the hidden `unmaintained` category for them:
- Apps that are listed in the `appdata-extractor/unmaintained_apps.yaml` or `appdata-extractor/unmaintained_projects.yaml` file are put in this category;
- Apps that have no category info in both their AppStream and Desktop files also go to this category, but we might want to add that info or manually set a category for them.

## Development
The Hugo part is a (Hu)Go module, thus requires both Hugo and Go to work. Read about the shared theme at [hugo-kde wiki](https://invent.kde.org/websites/hugo-kde/-/wikis/).

- Run commands:
```sh
# install hugo-i18n
git clone https://invent.kde.org/websites/hugo-i18n && pip3 install ./hugo-i18n
git clone git@invent.kde.org:websites/apps-kde-org && cd apps-kde-org
# install dependencies
pip3 install -r requirements.txt
python3 main.py
hugo server
```
- Open http://localhost:1313

### Notes
- The `app-id-matches.yaml` file contains matching IDs on Arch Linux and Snap Store of our apps.
  To generate or update this file, the converter's `gen_id_matches` function only needs to be run once in a while, based on the assumption that an app ID does not change after it is set.

## Maintainer
Jonathan Riddell, Carl Schwan, and Phu Nguyen. Contact us in #kde-www or #kde-devel :)

## License
This project is licensed under AGPL 3 or later. Individual files can be licensed under another compatible license.
